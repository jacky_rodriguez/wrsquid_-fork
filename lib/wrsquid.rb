require "wrsquid/version"
require "wrsquid/configuration"
require "wrsquid/sales_order"
require "wrsquid/item_allocation"
require "wrsquid/merchant"
require "httparty"
#require 'benchmark'


module Wrsquid
  module_function

  def load_config
      config_file = 'config/api_wrsquid_config.yml'
    if Dir.glob(config_file).any?
      YAML.load_file(config_file)
    else
      puts 'Initializing default configuration...'
    end
  end

  def configuration
    @configuration ||= Configuration.new(Wrsquid.load_config)
  end

  def filterize_content(default_content, content)
    # Use this to Avoid White space / Empty string
    content.to_s.strip.length == 0 ? default_content : content;
  end

  def channel_url(sub_link = "")
    File.join(configuration.channel_url, configuration.channel_id, filterize_content([], sub_link))
  end

end
  